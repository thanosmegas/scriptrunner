import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.ModifiedValue
import com.onresolve.scriptrunner.parameters.annotation.Checkbox
import com.onresolve.scriptrunner.parameters.annotation.Select
import com.onresolve.scriptrunner.parameters.annotation.ShortTextInput
import com.onresolve.scriptrunner.parameters.annotation.meta.Option
import org.apache.log4j.Logger
import org.apache.log4j.Level

// Set log level
def logline = Logger.getLogger(getClass())
logline.setLevel(Level.DEBUG)

//set custom field variable
final String customFieldName = 'NumOfSubTasks'

//Display form
@ShortTextInput(description = "The key of the issue to count the sub tasks", label = "Issue Key")
String issueKey

//retrieve issue
def issue = ComponentAccessor.issueManager.getIssueByCurrentKey(issueKey)

//if no issue found exit
if (issue == null) {
    logline.debug "there are no issue ${issueKey}"
    return
}

//count sub tasks
def Collection<Issue> subTasks = issue.getSubTaskObjects()
int count = 0;
for (Issue i : subTasks) {
      count++
}
def numOfSubTasks = count.toString()
logline.debug "there are ${numOfSubTasks} subtasks"

//get and update custom field value for issue
def customFieldManager = ComponentAccessor.customFieldManager
def customField = customFieldManager.getCustomFieldObjectsByName(customFieldName)
logline.debug "${customField}"

def customFieldId =		customFieldManager.getCustomFieldObjectByName(customFieldName).id
def customFieldValue =	customFieldManager.getCustomFieldObjectByName(customFieldName).getValue(issue)

def modifiedValue = new ModifiedValue(0 as Double, count as Double)
customFieldManager.getCustomFieldObjectByName(customFieldName).updateValue(null, issue, modifiedValue, new DefaultIssueChangeHolder())

logline.debug "custom Field Id = ${customFieldId} and value is ${customFieldValue} "

