import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.issue.IssueInputParameters
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.ModifiedValue
import com.onresolve.scriptrunner.parameters.annotation.Checkbox
import com.onresolve.scriptrunner.parameters.annotation.Select
import com.onresolve.scriptrunner.parameters.annotation.ShortTextInput
import com.onresolve.scriptrunner.parameters.annotation.meta.Option
import org.apache.log4j.Logger
import org.apache.log4j.Level

// Set log level
def logline = Logger.getLogger(getClass())
logline.setLevel(Level.DEBUG)

//Display form
@ShortTextInput(description = "The key of the issue to count the sub tasks", label = "Issue Key")
String issueKey

//retrieve issue
def issue = ComponentAccessor.issueManager.getIssueByCurrentKey(issueKey)

//if no issue found exit
if (issue == null) {
    logline.debug "there are no issue ${issueKey}"
    return
}

logline.debug "description was ${issue.getDescription()}"

def commentManager = ComponentAccessor.commentManager
def comment = commentManager.getLastComment(issue)
if (comment == null) {
    logline.debug "there are no comments on ${issueKey}"
    return
}
def lastcomment = comment.getBody()
logline.debug "last comment is ${lastcomment}"
issue.setDescription(lastcomment)
logline.debug "Description is ${issue.getDescription()}"

def issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService)
issue.store()
issueIndexingService.reIndex(issue)
